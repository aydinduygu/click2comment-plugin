package com.aydinduygu.click2comment;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import org.jetbrains.annotations.NotNull;

public class ConvertToCommentAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        // Get the selected text in the editor
        Editor editor = e.getData(CommonDataKeys.EDITOR);
        if (!isValid(e, editor)) return;

        editor.getSelectionModel().getSelectedText().lines().forEach(String::trim);

        // Convert the selected text to a comment
        String commentText = commentOrDecomment(editor.getSelectionModel().getSelectedText(), e);

        // Format the selected text
        int startOffset = editor.getSelectionModel().getSelectionStart();
        int endOffset = editor.getSelectionModel().getSelectionEnd();

        // Get the document and the line numbers for the start and end offsets
        Document document = editor.getDocument();

        Runnable replaceRunnable = () -> document.replaceString(startOffset, endOffset, commentText);
        WriteCommandAction.runWriteCommandAction(editor.getProject(), replaceRunnable);
    }

    private static boolean isValid(AnActionEvent e, Editor editor) {

        if (
                editor == null || editor.getSelectionModel().getSelectedText() == null || editor.getSelectionModel().getSelectedText().isEmpty()
        ) {
            e.getPresentation().setEnabled(false);
            return false;
        }
        return true;
    }

    @Override
    public void update(@NotNull AnActionEvent e) {
        // Get the selected text in the editor
        Editor editor = e.getData(CommonDataKeys.EDITOR);
        if (!isValid(e, editor)) return;

        editor.getSelectionModel().getSelectedText().lines().forEach(String::trim);
        updateButtonTextAndEnablement(editor.getSelectionModel().getSelectedText(), e);
    }

    @NotNull
    private static String commentOrDecomment(String selectedText, AnActionEvent event) {
        String commentText;
        if (selectedText.startsWith("/*") && selectedText.endsWith("*/")) {
            commentText = selectedText.replace("/*", "  ").replace("*/", "");
        } else if (selectedText.startsWith("//")) {
            commentText = selectedText.replace("//", "  ");
        } else {
            if (selectedText.lines().count() > 1) {
                commentText = "/*\n" + selectedText + "\n*/";
            } else {
                commentText = "//" + selectedText;
            }
        }
        return commentText;
    }

    @NotNull
    private static void updateButtonTextAndEnablement(String selectedText, AnActionEvent event) {
        if (selectedText.startsWith("//") || (selectedText.startsWith("/*") && selectedText.endsWith("*/"))) {
            event.getPresentation().setText("Undo Comment");
            event.getPresentation().setIcon(AllIcons.Actions.InlayRenameInComments);
        } else {
            event.getPresentation().setText("Click 2 Comment");
            event.getPresentation().setIcon(AllIcons.Actions.InlayRenameInCommentsActive);

        }
    }
}

